import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { ProfilePage } from '../pages/profile/profile';
import { SelectStudentPage } from '../pages/select-student/select-student';
import { EventsPage } from '../pages/events/events';
import { NoticeBoardPage } from '../pages/notice-board/notice-board';
import { TransportPage } from '../pages/transport/transport';
import { FeedbackPage } from '../pages/feedback/feedback';
import { ResultPage } from '../pages/result/result';
import { TimetableweekPage } from '../pages/timetableweek/timetableweek';
import { HomeworkPage } from '../pages/homework/homework';
import { DayTimeTablePage } from '../pages/day-time-table/day-time-table';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

