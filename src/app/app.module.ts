import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { AttendancePage } from '../pages/attendance/attendance';
import { NoticeBoardPage } from '../pages/notice-board/notice-board';
import { AnnualCalendarPage } from '../pages/annual-calendar/annual-calendar';
import { EventsPage } from '../pages/events/events';
import { HomeworkPage } from '../pages/homework/homework';
import { ResultPage } from '../pages/result/result';
import { SelectStudentPage } from '../pages/select-student/select-student';
import { TransportPage } from '../pages/transport/transport';
import { FeedbackPage } from '../pages/feedback/feedback';
import { TimetableweekPage } from '../pages/timetableweek/timetableweek';
import { DayTimeTablePage } from '../pages/day-time-table/day-time-table';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChangePasswordPage,
    LoginPage,
    ProfilePage,
    AttendancePage,
    NoticeBoardPage,
    AnnualCalendarPage,
    EventsPage,
    HomeworkPage,
    ResultPage,
    SelectStudentPage,
    TransportPage,
    FeedbackPage,
    TimetableweekPage,
    DayTimeTablePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChangePasswordPage,
    LoginPage,
    ProfilePage,
    AttendancePage,
    NoticeBoardPage,
    AnnualCalendarPage,
    EventsPage,
    HomeworkPage,
    ResultPage,
    SelectStudentPage,
    TransportPage,
    FeedbackPage,
    TimetableweekPage,
    DayTimeTablePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
