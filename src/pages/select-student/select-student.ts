import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the SelectStudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-student',
  templateUrl: 'select-student.html',
})
export class SelectStudentPage {
  genderSelection : any='0';
  imglayoutchange1: string;
  imglayoutchange2: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectStudentPage');
  }
  
  boyselect(){
    this.imglayoutchange1= "imglayoutborder";
    this.imglayoutchange2= "";
    this.genderSelection = 'boy';
  }
  girlselect(){
    this.imglayoutchange2= "imglayoutborder";
    this.imglayoutchange1= "";
    this.genderSelection = 'girl';
  }
  select(){
    if(this.genderSelection == "boy"){
        
    }
    else if(this.genderSelection == "girl"){

    }
    else{

    }
    this.navCtrl.setRoot(HomePage);
  }
}
